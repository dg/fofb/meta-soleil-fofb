#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdint.h>
#include <fcntl.h>
#include <ChimeraTK/Device.h>
#include <errno.h>
#include <ctime>


int main(){

    int fd_ddr, fd_ddr_sro;     // File descriptors for DDR4
    int fd_dst;                 // File descriptor for destination
    unsigned int fcnt=0;        // Binary file counter
    int rp;
    int i;
    ssize_t s_wr;
    const uint32_t unmask = 1, mask = 0;
    uint32_t info;
    char fname[128];
    char fdate[32];
    time_t t;
    char *src;
    uint8_t cur_buf;
    struct pollfd pfd;

    // Initialize ChimeraTK ===================================================
    ChimeraTK::setDMapFilePath("/opt/fofb/map/appuio.dmap");
    ChimeraTK::Device dev("APPUIO");
    dev.open();

    // Declare register accessors
    ChimeraTK::ScalarRegisterAccessor<uint32_t> daq_enable =
        dev.getScalarRegisterAccessor<uint32_t>("APP/daq_0/ENABLE");
    ChimeraTK::ScalarRegisterAccessor<uint32_t> daq_dbuf_ena =
        dev.getScalarRegisterAccessor<uint32_t>("APP/daq_0/DOUBLE_BUF_ENA");
    ChimeraTK::ScalarRegisterAccessor<uint32_t> daq_trigger =
        dev.getScalarRegisterAccessor<uint32_t>("APP/DAQ_CONTROL");
    ChimeraTK::OneDRegisterAccessor<uint32_t> daq_tabsel =
        dev.getOneDRegisterAccessor<uint32_t>("APP/daq_0/TAB_SEL");
    ChimeraTK::ScalarRegisterAccessor<uint32_t> daq_curbuff =
        dev.getScalarRegisterAccessor<uint32_t>("APP/daq_0/ACTIVE_BUF");

    // Open DDR access ========================================================
    // Open file descriptor for DDR4
    fd_ddr = open("/dev/ddr4", O_RDWR);
    fd_ddr_sro = open("/dev/ddr4", O_RDONLY | O_SYNC);
    if (fd_ddr <= 0 || fd_ddr_sro <= 0) {
        fprintf(stderr, "Failed to open UIO device.\n");
        return -1;
    };
    pfd.fd = fd_ddr;
    pfd.events = POLLIN;

    // Open MMAP for DDR4
    src = static_cast<char*>(mmap(NULL, 0x20000000, PROT_READ, MAP_SHARED, fd_ddr_sro, 0));
    if (src == MAP_FAILED) {
        fprintf(stderr, "Failed to MMAP the DDR: %s.\n", strerror(errno));
        return -9;
    };

    // Initialize DAQ =========================================================
    // Disable DAQ
    daq_enable = 0;
    daq_enable.write();

    // Flushing interrupts
    //*
    rp = 1;
    while (rp >= 1) {
        fprintf(stdout, "Flushing interrupt...\n");

        //  unmask irq
        s_wr = write(fd_ddr, &unmask, sizeof(unmask));
        if (s_wr != (ssize_t)sizeof(unmask)) {
            fprintf(stderr, "Error when unmasking interrupt !\n");
            return -3;
        };

        rp = poll(&pfd, 1, 1000);

        if (rp >= 1) {
            read(fd_ddr, &info, sizeof(info));
        };
    };
    // */

    // Masking interrupt
    s_wr = write(fd_ddr, &mask, sizeof(mask));
    if (s_wr != (ssize_t)sizeof(mask)) {
        fprintf(stderr, "Error when masking interrupt !\n");
        return -3;
    };

    // Configure DAQ
    daq_dbuf_ena = 3;
    daq_dbuf_ena.write();
    daq_tabsel[0] = 1;
    daq_tabsel[1] = 1;
    daq_tabsel.write();

    // Enable DAQ
    daq_enable = 1;
    daq_enable.write();
    daq_curbuff.read();
    cur_buf = daq_curbuff & (uint32_t)(1);

    // trigger
    daq_trigger = 1;
    daq_trigger.write();
    daq_trigger = 0;
    daq_trigger.write();

    // ========================================================================

    while(fcnt < 5) {
        // Unmask interrupt
        s_wr = write(fd_ddr, &unmask, sizeof(unmask));
        if (s_wr != (ssize_t)sizeof(unmask)) {
            fprintf(stderr, "Error when unmasking interrupt !\n");
            return -3;
        };

        // Wait for interrupt
        fprintf(stdout, "Waiting for interrupt...\n");
        rp = poll(&pfd, 1, -1);     // Timeout = -1, infinite wait

        if (rp >= 1) {
            read(fd_ddr, &info, sizeof(info));
            fprintf(stdout, "Read interrupt %d\n", info);
        } else if (rp == 0) {
            fprintf(stderr, "Wait for IRQ interrupted, timeout or signal.\n");
            return -4;
        } else {
            fprintf(stderr, "Failed poll.\n");
            return -5;
            //fprintf(stderr, "Failed poll. Relaunch for next iteration.\n");
            //continue;
        };

        // Ignore first one
        fcnt += 1;
        if (fcnt == 1) {
            continue;
        }

        // Open destination file
        t=time(0);
        strftime(fdate, 32, "%Y%m%d_%H%M%S", localtime(&t));
        snprintf(fname, 128, "/mnt/data/archive_files/file_%s.bin", fdate);
        fd_dst = open(fname, O_CREAT | O_WRONLY | O_APPEND , 0666);
        if (fd_dst <= 0) {
            fprintf(stderr, "Failed to open destination file '%s'.\n", fname);
            return -2;
        };

        fprintf(stdout, "Write to file '%s'.\n", fname);
        // Copy data, whole block
        /*
        s_wr = write(fd_dst, &src, 0x08000000);
        if (s_wr < 0x08000000) {
            fprintf(stderr, "Error while writing to file '%s' (%d).\n", fname, s_wr);
            return -6;
        };
        // */

        // Copy data, page by page
        //*
        for (i = 0 ; i < 0x08000 ; i++) {
            s_wr = write(fd_dst, &src[i*0x1000+cur_buf*0x08000000], 0x1000);
            if (s_wr < 0x1000) {
                fprintf(stderr, "Error while writing to file '%s' (%d:%d).\n", fname, i, s_wr);
                return -6;
            };
        };
        // */

        // Copy data, element by element
        /*
        for (i=0 ; i < 0x00800000 ; i++) {
            s_wr = write(fd_dst, &src[i*16 + 0 + cur_buf*0x08000000], 4);
            s_wr = s_wr+ write(fd_dst, &src[i*16 + 4 + cur_buf*0x08000000], 4);
            s_wr = s_wr+ write(fd_dst, &src[i*16 + 8 + cur_buf*0x08000000], 1);
            s_wr = s_wr+ write(fd_dst, &src[i*16 + 12 + cur_buf*0x08000000], 1);
            if (s_wr < 10) {
                fprintf(stderr, "Error while writing to file '%s' (%d:%d).\n", fname, i, s_wr);
                return -6;
            };
        };
        */


        cur_buf = (cur_buf+1)%2;

        close(fd_dst);

    };

    // Disable DAQ, mask IRQ
    daq_enable = 0;
    daq_enable.write();
    s_wr = write(fd_ddr, &mask, sizeof(mask));

    fprintf(stdout, "Done !\n");

    return 0;

}
