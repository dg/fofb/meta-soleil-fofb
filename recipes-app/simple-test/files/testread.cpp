#include <ChimeraTK/Device.h>

int main(){
  ChimeraTK::setDMapFilePath("soleil-fofb.dmap");
  ChimeraTK::Device dev("CNUIO");
  dev.open();

  auto acc = dev.getScalarRegisterAccessor<uint32_t>("ccn_packeter_0.VERSION");
  acc.read();
}
