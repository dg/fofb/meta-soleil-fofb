SUMMARY = "Simple python tool to use DESY DAQ"
SECTION = "opt"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " file://daqcapt"

RDEPENDS_${PN}=" deviceaccess-mapfiles deviceaccess-python-bindings"

FILES_${PN}+="/usr/bin/daqcapt"
FILES_${PN}+="/usr/bin/daqarchiver"


do_install() {
    # Add FPGA version reader and FoFb Configurator
    install -d ${D}/usr/bin/
    install -m 0755 ${WORKDIR}/daqcapt ${D}/usr/bin/daqcapt
    install -m 0755 ${WORKDIR}/daqcapt ${D}/usr/bin/daqarchiver
}
