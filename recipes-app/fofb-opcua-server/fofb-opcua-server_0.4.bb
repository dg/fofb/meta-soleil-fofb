SUMMARY = "Add an ChimeraTK OPCUA-GenericServer to access PL app"
SECTION = "opt"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"


# This will add startup scripts
INITSCRIPT_NAME = "fofb-opcua-server.sh"
INITSCRIPT_PARAMS = "defaults 91 3"
inherit update-rc.d

SRC_URI = " \
            file://generic_chimeratk_server_configuration.xml \
            file://fofb-opcua-server.sh \
"

DEPENDS=" deviceaccess-mapfiles"
RDEPENDS_${PN}=" genericdeviceserver-opcua bash"

FILES_${PN}+="/opt/fofb/opcua-server/*"
FILES_${PN}+="/etc/init.d/fofb-opcua-server.sh"

do_install() {
    install -d ${D}/opt/fofb/opcua-server/
    install -m 0644 ${WORKDIR}/generic_chimeratk_server_configuration.xml ${D}/opt/fofb/opcua-server/generic_chimeratk_server_configuration.xml

    install -d ${D}/etc/init.d/
    install -m 0755 ${WORKDIR}/fofb-opcua-server.sh ${D}/etc/init.d/fofb-opcua-server.sh
}
