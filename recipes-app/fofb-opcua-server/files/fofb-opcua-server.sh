#!/bin/bash

. /etc/init.d/functions

PATH_CFG="/opt/fofb/cfg/"
PATH_OPCUA="/opt/fofb/opcua-server/"
PATH_MAP="/opt/fofb/map/"
LOG_FILE=/var/log/fofb-opcua-server.log
LOCK_FILE=/var/lock/subsys/fofb-opcua-server

# Reading the configuration file
CFG_FILE=${PATH_CFG}/configuration
echo "Reading configuration file ${CFG_FILE}" >> $LOG_FILE
if [ ! -f ${CFG_FILE} ]; then
    echo "Configuration file not found: ${CFG_FILE}" >> $LOG_FILE
    exit 1
fi
source ${CFG_FILE}

# Start the service
start() {
    if [ -f $LOCK_FILE ]; then
        echo "fofb-opcua server already running, status: $(cat $LOCK_FILE)" >> $LOG_FILE
        exit 1
    fi

    echo "Starting fofb-opcua-server" >> $LOG_FILE
    cd $PATH_OPCUA

    # Link to devices.dmap
    ln -sf ${PATH_MAP}/devices.dmap devices.dmap

    # Start server
    echo "starting" > $LOCK_FILE
    stdbuf -oL opcua-generic-chimeratk-server01 &>> $LOG_FILE &

    ret=$( tail -n0 -f $LOG_FILE | awk '/logic_error/{print 0; exit} /All application modules are running/{print 1;exit}' )

    if [ "$ret" -eq "0" ]; then
        echo "crashed" > $LOCK_FILE
        failure
    else
        echo "started" > $LOCK_FILE
        success
    fi
}


# Restart the service
stop() {
    echo "Stopping fofb-opcua-server" >> $LOG_FILE
    killproc opcua-generic-chimeratk-server01
    ### Now, delete the lock file ###
    rm -f $LOCK_FILE
}

status() {
    if [ -f $LOCK_FILE ]; then
        echo "fofb-opcua server status: $(cat $LOCK_FILE)"
    else
        echo "fofb-opcua server status: stopped"
    fi
}


### main logic ###
case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        status
        ;;
    restart|reload|condrestart)
        stop
        start
        ;;
    *)
        echo $"Usage: $0 {start|stop|restart|reload|status}"
        exit 1
esac

exit 0



