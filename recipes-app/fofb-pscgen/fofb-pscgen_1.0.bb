SUMMARY = "Simple python tool to use the PSC sinus generator"
SECTION = "opt"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " file://pscgen"

RDEPENDS_${PN}=" deviceaccess-mapfiles deviceaccess-python-bindings"

FILES_${PN}+="/usr/bin/pscgen"

do_install() {
    # Add FPGA version reader and FoFb Configurator
    install -d ${D}/usr/bin/
    install -m 0755 ${WORKDIR}/pscgen ${D}/usr/bin/pscgen
}
