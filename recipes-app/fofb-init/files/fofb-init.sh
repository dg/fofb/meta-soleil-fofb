#!/bin/bash

. /etc/init.d/functions

PATH_CFG="/opt/fofb/cfg/"
PATH_MAP="/opt/fofb/map/"
PATH_MAPT="/usr/share/deviceaccess/map/"
PATH_FPGABIN=${PATH_CFG}/fpga_bitstream.bin
LOCK_FILE=/var/lock/subsys/fofb-init


shopt -s expand_aliases
alias log="logger -t \"fofb-fpgainit\""

# Reading the configuration file
CFG_FILE=${PATH_CFG}/configuration
log "Reading configuration file ${CFG_FILE}"
if [ ! -f ${CFG_FILE} ]; then
    log -p user.error "Configuration file not found: ${CFG_FILE}"
    exit 1
fi
source ${CFG_FILE}

########################################################################
# Link the proper FPGA bitstream depending on the configuration variable FOFB_APP
link_fpga_bitstream() {
    if [ -z FOFB_APP ]; then
        log -p user.error "Variable FOFB_APP not set in configuration file ${CFG_FILE}"
        exit 1
    fi

    PATH_TARGET=/lib/firmware/base/${FOFB_APP}/pl-full.bit.bin

    if [ ! -e $PATH_TARGET ]; then
        log -p user.error "Could not establish FOFB application"
        exit 1
    fi

    log "Linking FPGA bitstream ${PATH_TARGET}"
    ln -sf ${PATH_TARGET} ${PATH_FPGABIN}
}

########################################################################
# Link the proper MAPT file depending on FOFB_APP
link_mapt() {
    if [ -z FOFB_APP ]; then
        log -p user.error "Variable FOFB_APP not set in configuration file ${CFG_FILE}"
        exit 1
    fi

    LINK_TARGET="${PATH_MAPT}/${FOFB_APP}/${FOFB_APP}_ch8.mapt"
    if [ ! -f ${LINK_TARGET} ]; then
        log -p user.error "MAPT file not found: ${LINK_TARGET}"
        exit 1
    fi

    log "Linking map file ${LINK_TARGET}"
    ln -sf ${LINK_TARGET} ${PATH_MAP}/app.mapt

}

########################################################################
# Link the proper XLMAP file depending on FOFB_APP
link_xlmap() {
    if [ -z FOFB_APP ]; then
        log -p user.error "Variable FOFB_APP not set in configuration file ${CFG_FILE}"
        exit 1
    fi

    if [[ ${FOFB_APP} = *"Central"* ]]; then
        log "CentralNode application detected"
        ln -sf ${PATH_MAP}/lnm_central.xlmap ${PATH_MAP}/lnm.xlmap
    fi

    if [[ ${FOFB_APP} = *"Cell"* ]]; then
        log "CellNode application detected"
        ln -sf ${PATH_MAP}/lnm_cell.xlmap ${PATH_MAP}/lnm.xlmap
    fi

}

########################################################################
# Link the proper configuration register
link_configuration() {
    if [ -z FOFB_CFG ]; then
        log -p user.error "Variable FOFB_CFG not set in configuration file ${CFG_FILE}"
        exit 1
    fi

    LINK_TARGET="${PATH_CFG}/${FOFB_CFG}_config_register"
    if [ ! -f ${LINK_TARGET} ]; then
        log -p user.error "Register Configuration file not found: ${LINK_TARGET}"
        exit 1
    fi

    log "Linking config_register file ${LINK_TARGET}"
    ln -sf ${LINK_TARGET} ${PATH_CFG}/config_register
}

########################################################################
# Write Bitstream
# Reset PS
# Apply register configuration
fpga_reconfig() {
    log "Loading FPGA image ${PATH_FPGABIN}"
    fpgautil -R |& log

    # First without overlay, it stall if not
    fpgautil -b ${PATH_FPGABIN} |& log

    fpgautil -b ${PATH_FPGABIN} -o ${PATH_MAP}/device-tree-overlay.dtbo |& log

    log "Reset the FPGA"
    /etc/init.d/fw_plreset.sh |& log

    log "Start mmcctrl"
    start-stop-daemon -S mmcctrld |& log

    # Applying configuration
    fofb-configurator --config ${PATH_CFG}/config_register --dmap ${PATH_MAP}/devices.dmap |& log
}

########################################################################
########################################################################
########################################################################
# Daemon style handling

reload() {
    if [ ! -f $LOCK_FILE ]; then
        echo "FOFB application not started, cannot reload"
        exit 1
    fi

    log "FOFB application: loading application"
    fpga_reconfig
    echo "started" > $LOCK_FILE
}

start() {
    if [ -f $LOCK_FILE ]; then
        echo "FOFB application already started, status: $(cat $LOCK_FILE)"
        exit 1
    fi

    log "FOFB application: linking configuration"

    link_fpga_bitstream
    link_mapt
    link_xlmap
    link_configuration

    echo "configured" > $LOCK_FILE

    reload
}

stop() {
    log "FOFB application: (not really) stopping"
    rm -f $LOCK_FILE
}

status() {
    if [ -f $LOCK_FILE ]; then
        echo "fofb-opcua server status: $(cat $LOCK_FILE)"
    else
        echo "fofb-opcua server status: stopped"
    fi
}

### main logic ###
case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        status
        ;;
    reload)
        reload
        ;;
    restart|condrestart)
        stop
        start
        ;;
    *)
        echo $"Usage: $0 {start|stop|restart|reload|status}"
        exit 1
esac



exit 0
