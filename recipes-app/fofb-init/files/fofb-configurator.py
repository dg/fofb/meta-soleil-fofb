#!/usr/bin/env python3
import deviceaccess as da
import numpy as np
import logging
import argparse

#######################################################################
#       MODULE CONFIGURATION
#######################################################################
# Logger
logger = logging.getLogger("fofb-configurator")
logger.setLevel(logging.INFO)
sh = logging.StreamHandler()
sh.setLevel(logging.INFO)
sh.setFormatter(logging.Formatter('%(name)s - %(levelname)s - %(message)s'))
logger.addHandler(sh)

# Argparse
parser=argparse.ArgumentParser(
        prog="fofb-configurator",
        description="Apply a default configuration file to a ChimeraTK device."
        )

parser.add_argument(
        "--config", help="Path to configuration file (%(default)s).",
        default="/opt/fofb/cfg/regconfig",)

parser.add_argument(
        "--dmap", help="ChimeraTK DMAP file (%(default)s).",
        default="/opt/fofb/opcua-server/devices.dmap",)

parser.add_argument(
        "--devname", help="ChimeraTK device name (%(default)s).",
        default="APPUIO",)


#######################################################################
#       GET DEVICE ACCESS
#######################################################################
def GetDeviceAccess(dmappath, devicename):
    """
    Open the device access
    """
    logger.info("Openning device {} in {}.".format(devicename, dmappath))

    da.setDMapFilePath(dmappath)
    dev=da.Device(devicename)

    dev.open()

    return dev


#######################################################################
#       READ CONFIG FILE
#######################################################################
def ReadConfigFile(filepath):
    """
    Read the configuration file, building a dictionnary
    """
    logger.info("Reading configuration file {}.".format(filepath))

    dcfg={}
    with open(filepath, 'r') as fp:

        for i, l in enumerate(fp.readlines()):
            # Ignore empty or comment
            if len(l.strip()) <1:
                continue
            if l.strip().startswith('#'):
                continue

            # Try to split in two, or pass
            try:
                k,v = l.split()
            except ValueError:
                logger.warning("{} ignore line {}:'{}'".format(filepath, i, l))
                continue

            # Understand hex format
            if v.lower().startswith("0x"):
                base=16
            else:
                base=10

            # Try to cast value, or pass
            try:
                v = int(v, base)
            except ValueError:
                logger.warning("{} ignore line {}:'{}'".format(filepath, i, l))
                continue

            dcfg[k]=v

    return dcfg

#######################################################################
#        APPLY CONFIG
#######################################################################
def ApplyConfig(dev, config):
    """
    Apply dictionnary config to device dev.
    """

    for k, v in config.items():

        try:
            acc = dev.getScalarRegisterAccessor(np.uint32, k)
            acc.set(np.asarray([v], dtype=np.uint32))
            acc.write()
        except:
            logger.error("Could not apply value {} to {}".format(v,k))
            raise

        logger.info("Applied  {} = {}".format(k,v))



#######################################################################
#        MAIN
#######################################################################
if __name__ == '__main__':

    args = parser.parse_args()

    # Testing logger
    logger.info("Welcome to FOFB-CONFIGURATOR")

    try:
        d=ReadConfigFile(args.config)
    except FileNotFoundError:
        logger.error("File not found: {}".format(args.config))
        exit(1)

    try:
        dev=GetDeviceAccess(args.dmap, args.devname)
    except Exception:
        logger.error("Cannot open Device {} in {}".format(args.devname, args.dmap))
        raise

    ApplyConfig(dev, d)

    dev.close()


