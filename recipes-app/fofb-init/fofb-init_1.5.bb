SUMMARY = "Initialize the system for FOFB application"
SECTION = "opt"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# This will add startup scripts
INITSCRIPT_NAME = "fofb-init.sh"
INITSCRIPT_PARAMS = "defaults 50 2"
inherit update-rc.d

SRC_URI = " file://fofb-init.sh"
SRC_URI += " file://fpgaversion.sh"
SRC_URI += " file://fofb-configurator.py"
SRC_URI += " file://configuration.example"
SRC_URI += " file://config"
SRC_URI += " file://lnm_central.xlmap "
SRC_URI += " file://lnm_cell.xlmap "
SRC_URI += " file://devices.dmap "

DEPENDS=" deviceaccess-mapfiles"
RDEPENDS_${PN}=" deviceaccess-mapfiles fpga-manager-script bash"

FILES_${PN}+="/etc/init.d/fofb-init.sh"
FILES_${PN}+="/opt/fofb/map/*"
FILES_${PN}+="/usr/bin/fpgaversion"
FILES_${PN}+="/usr/bin/fofb-configurator"
FILES_${PN}+="/opt/fofb/cfg/configuration.example"
FILES_${PN}+="/opt/fofb/cfg/C01_config_register /opt/fofb/cfg/C06_config_register /opt/fofb/cfg/C09_config_register /opt/fofb/cfg/C14_config_register /opt/fofb/cfg/Central_config_register"


do_install() {
    install -d ${D}/etc/init.d/
    install -m 0755 ${WORKDIR}/fofb-init.sh ${D}/etc/init.d/fofb-init.sh

    # Add FPGA version reader and FoFb Configurator
    install -d ${D}/usr/bin/
    install -m 0755 ${WORKDIR}/fpgaversion.sh ${D}/usr/bin/fpgaversion
    install -m 0755 ${WORKDIR}/fofb-configurator.py ${D}/usr/bin/fofb-configurator

    install -d ${D}/opt/fofb/cfg
    install ${WORKDIR}/config/*_config_register -t ${D}/opt/fofb/cfg
    install ${WORKDIR}/configuration.example ${D}/opt/fofb/cfg/configuration.example

    install -d ${D}/opt/fofb/map
    install -m 0644 ${WORKDIR}/lnm_central.xlmap ${D}/opt/fofb/map/lnm_central.xlmap
    install -m 0644 ${WORKDIR}/lnm_cell.xlmap ${D}/opt/fofb/map/lnm_cell.xlmap
    install -m 0644 ${WORKDIR}/devices.dmap ${D}/opt/fofb/map/devices.dmap
}
