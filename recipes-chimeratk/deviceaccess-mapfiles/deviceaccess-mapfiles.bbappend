MAPFILES_SRC_ORIG = "${RECIPE_SYSROOT}/opt/xilinx/hw-design"
MAPFILES_SRC_BASE = "${WORKDIR}/map"

# Changing BAR of mapt files to 0 (field #5 receive 0)
# Replace each line of MATRIX coeff by a global entry
do_compile () {
    if [ "${PL_VARIANTS}" != "" ]; then
        for PL_VARIANT in ${PL_VARIANTS}; do
            VAR_SRCDIR=${MAPFILES_SRC_ORIG}/${PL_VARIANT}
            VAR_DESTDIR=${MAPFILES_SRC_BASE}/${PL_VARIANT}
            mkdir -p ${VAR_DESTDIR}

            for MAPFILE_SRC in ${VAR_SRCDIR}/*.map[pt]; do
                echo "Transform file ${MAPFILE_SRC}"
                awk 'FNR>1 {$5=0}
                    {if ($1 ~ /MATRIXCOEF\.[^0]/ ) next}
                    {if ($1 ~ /MATRIXCOEF\.0/) {$1="APP.corr_matrix_0.MATRIXCOEF" ; $2="25600" ; $4="102400"}}
                    {print}' ${MAPFILE_SRC} > ${VAR_DESTDIR}/$(basename ${MAPFILE_SRC})
            done
        done
    else
        HDF_BASENAME=$(echo ${HDF_SUFFIX} | cut -c2-)
        VAR_DESTDIR=${D}${MAPFILES_DST_BASE}/${HDF_BASENAME}
        for MAPFILE_SRC in ${MAPFILES_SRC_BASE}/*.map[pt]; do
            echo "Transform file ${MAPFILE_SRC}"
            awk 'FNR>1 {$5=0}
                {if ($1 ~ /MATRIXCOEF\.[^0]/ ) next}
                {if ($1 ~ /MATRIXCOEF\.0/) {$1="APP.corr_matrix_0.MATRIXCOEF" ; $2="25600" ; $4="102400"}}
                {print}' ${MAPFILE_SRC} > ${VAR_DESTDIR}/$(basename ${MAPFILE_SRC})
        done
    fi

}
