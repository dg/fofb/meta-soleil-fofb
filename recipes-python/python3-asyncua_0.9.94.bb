SUMMARY = "Pure Python OPC-UA client and server library"
HOMEPAGE = "https://github.com/FreeOpcUa/opcua-asyncio"
LICENSE = "LGPL-3.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=e6a600fd5e1d9cbde2d983680233ad02"
SRC_URI[md5sum]= "3d58ec12bad1112b25f876ac67e9ccbc"

PYPI_PACKAGE = "asyncua"

inherit pypi setuptools3

RDEPENDS_${PN} ="\
    ${PYTHON_PN}-aiofiles \
    ${PYTHON_PN}-importlib-metadata \
    ${PYTHON_PN}-cryptography \
    ${PYTHON_PN}-aiosqlite \
    ${PYTHON_PN}-dateutil \
    ${PYTHON_PN}-pytz \
    ${PYTHON_PN}-sortedcontainers \
    "
