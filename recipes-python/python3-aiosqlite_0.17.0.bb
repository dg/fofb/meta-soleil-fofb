SUMMARY = "aiosqlite provides a friendly, async interface to sqlite databases"
HOMEPAGE = "https://github.com/omnilib/aiosqlite"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=2066c5c20642aa773df4a6dced7611c2"
SRC_URI[md5sum]= "6d329ca418d7df462750640366515b11"

PYPI_PACKAGE = "aiosqlite"

inherit pypi setuptools3
