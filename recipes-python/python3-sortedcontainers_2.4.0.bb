SUMMARY = "Sorted Containers -- Sorted List, Sorted Dict, Sorted Set"
HOMEPAGE = "https://github.com/grantjenks/python-sortedcontainers"
LICENSE = "Apache-2"
LIC_FILES_CHKSUM = "file://LICENSE;md5=7c7c6a1a12ec816da16c1839137d53ae"
SRC_URI[md5sum]= "50eeb6cb739568b590b28f9a3f445c78"

PYPI_PACKAGE = "sortedcontainers"

inherit pypi setuptools3
