SUMMARY = "Python binding for Tango, a library dedicated to distributed control systems."
HOMEPAGE = "https://gitlab.com/tango-controls/pytango"
LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=e6a600fd5e1d9cbde2d983680233ad02"
SRC_URI[md5sum]= "23fe4082a6478c7ae56fdbc3bc806e7d"

PYPI_PACKAGE = "pytango"

inherit pypi setuptools3
