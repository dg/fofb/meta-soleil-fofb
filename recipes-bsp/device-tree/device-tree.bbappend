FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI_append = " \
    file://pl-over.dtsi \
"

do_configure_append() {

    # Add the overlay source to DTS sources
    cp ${WORKDIR}/pl-over.dtsi ${DT_FILES_PATH}/

}
