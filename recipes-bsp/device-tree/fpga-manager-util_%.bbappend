
FILES_${PN}+="/opt/fofb/map/device-tree-overlay.dtbo"

do_install_prepend() {

    install -d ${D}/opt/fofb/map
    install -m 0644 ${RECIPE_SYSROOT}/boot/devicetree/pl-over.dtbo ${D}/opt/fofb/map/device-tree-overlay.dtbo
}
