DESCRIPTION = "FOFB system for DAMC-FMC2ZUP"

COMPATIBLE_MACHINE = "damc-fmc2zup"

# disable autologin at startup on serial tty
IMAGE_AUTOLOGIN = "0"

# Based on techlab headless image
require recipes-core/images/techlab-headless.inc

# Include ZUP common
# this deploy HW packages for zup, zup-demo package
# and some applications (htop, vim, nano, python3...)
require ./recipes-core/images/techlab-common.inc

# Add features
IMAGE_FEATURES_append = " \
    package-management \
    petalinux-python-modules \
    debug-tweaks \
    "

# Add applications
IMAGE_INSTALL_append = " packagegroup-zup-support"
IMAGE_INSTALL_append = " util-linux glibc-utils sudo"
IMAGE_INSTALL_append = " python3-asyncua"
IMAGE_INSTALL_append = " deviceaccess"
IMAGE_INSTALL_append = " deviceaccess-python-bindings"
IMAGE_INSTALL_append = " fofb-opcua-server"
IMAGE_INSTALL_append = " fofb-init"
IMAGE_INSTALL_append = " fofb-daqcapt fofb-pscgen"
IMAGE_INSTALL_append = " nfs-utils"

# We do not need that
IMAGE_INSTALL_remove = " git python3-pip sudo"
IMAGE_INSTALL_remove = " fpgautil-init"

DISTRO_FEATURES_append = "nfs"

# Remove graphical features
DISTRO_FEATURES_remove = " x11 wayland opengl qt jupyter"
# Not sure the following is usefull...
IMAGE_FEATURES_remove = " x11 wayland opengl qt jupyter"

# Remove unwanted features
DISTRO_FEATURES_remove = " wifi bluetooth 3g nfc"

# User
inherit extrausers
EXTRA_USERS_PARAMS = "\
    usermod -s /bin/zsh -p '\$6\$AzsdzifDHR4dpNHF\$MtXMd75T2CNGqilFDVeYxSQknBsr4ykeAxwOgTC9yeKfeJjKG5kf5l9TKam1yMht50XL0bzE1AhRQtbaMHUmB1' root; \
    useradd -s /bin/zsh -G sudo -p '\$6\$hmp0ah1eA0dKl795\$5/m5u0Xuvz8.Elik9YH3oQpM2fyn6em0mmHKyJgt7CVUayCEFc38BIMKQstDrHuE0IbuDHDzApxsD7jwXHwKI0' diag; \
    "
